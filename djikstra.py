import xml.etree.ElementTree as ET
import numpy as np
import cv2
import math
import sys

class Vertex:
    def __init__(self, node):
        self.id = node
        '''self.lat = node.lat
        self.lon = node.lon'''
        self.adjacent = {}
        # Set distance to infinity for all nodes
        self.distance = sys.maxsize
        # Mark all nodes unvisited
        self.visited = False
        # Predecessor
        self.previous = None

    def add_neighbor(self, neighbor, weight):
        self.adjacent[neighbor] = weight

    def get_connections(self):
        return self.adjacent.keys()

    def get_id(self):
        return self.id

    def get_weight(self, neighbor):
        return self.adjacent[neighbor]

    def set_distance(self, dist):
        self.distance = dist

    def get_distance(self):
        return self.distance

    def set_previous(self, prev):
        self.previous = prev

    def set_visited(self):
        self.visited = True

    def __str__(self):
        return str(self.id) + ' adjacent: ' + str([x.id for x in self.adjacent])


    def __lt__(self, other):
        return ((self.distance) < (other.distance))

class Graph:
    def __init__(self):
        self.vert_dict = {}
        self.num_vertices = 0

    def __iter__(self):
        return iter(self.vert_dict.values())

    def add_vertex(self, node):
        self.num_vertices = self.num_vertices + 1
        new_vertex = Vertex(node)
        self.vert_dict[node] = new_vertex
        return new_vertex

    def get_vertex(self, n):
        if n in self.vert_dict:
            return self.vert_dict[n]
        else:
            return None

    def add_edge(self, frm, to, cost):
        if frm not in self.vert_dict:
            self.add_vertex(frm)
        if to not in self.vert_dict:
            self.add_vertex(to)

        self.vert_dict[frm].add_neighbor(self.vert_dict[to], float(cost))
        self.vert_dict[to].add_neighbor(self.vert_dict[frm], float(cost))

    def get_vertices(self):
        return self.vert_dict.keys()

    def set_previous(self, current):
        self.previous = current

    def get_previous(self, current):
        return self.previous

def shortest(v, path):
    ''' make shortest path from v.previous'''
    if v.previous:
        path.append(v.previous.get_id())
        shortest(v.previous, path)
    return

import heapq

def dijkstra(aGraph, start, target):
    #print 'Dijkstra's shortest path'
    # Set the distance for the start node to zero
    start.set_distance(0)

    # Put tuple pair into the priority queue
    unvisited_queue = [(v.get_distance(),v) for v in aGraph]
    heapq.heapify(unvisited_queue)

    while len(unvisited_queue):
        # Pops a vertex with the smallest distance
        uv = heapq.heappop(unvisited_queue)
        current = uv[1]
        current.set_visited()

        #for next in v.adjacent:
        for next in current.adjacent:
            # if visited, skip
            if next.visited:
                continue
            new_dist = current.get_distance() + current.get_weight(next)

            if new_dist < next.get_distance():
                next.set_distance(new_dist)
                next.set_previous(current)

        # Rebuild heap
        # 1. Pop every item
        while len(unvisited_queue):
            heapq.heappop(unvisited_queue)
        # 2. Put all vertices not visited into the queue
        unvisited_queue = [(v.get_distance(),v) for v in aGraph if not v.visited]
        heapq.heapify(unvisited_queue)


class Node:
    def __init__(self, lat, lon, id = 0):
        self.lat = lat
        self.lon = lon
        self.id = id

class Stop:
    def __init__(self, node, name):
        self.node = node
        self.name = name

class Way:
    def __init__(self, streetName):
        self.nodes = []
        self.streetName = streetName

    def addNode(self, node):
        self.nodes.append(node)



def getAllStops(element, nodesAll):
    stopsArr = []
    for node in element.findall('node'):
        lat = float(node.get('lat'))
        lon = float(node.get('lon'))
        tags = node.findall('tag')

        kArr = []
        vArr = []

        for tag in tags:
            kArr.append(tag.get('k'))
            vArr.append(tag.get('v'))

        if 'stop_position' in vArr and 'name' in kArr:
            stopNode = Node(lat, lon)
            nodesAll.append(stopNode)
            stopsArr.append(Stop(stopNode, vArr[kArr.index('name')]))
    return stopsArr

def getWays(element, nodesAll, nodesWays):
    residential = []
    secondary = []
    primary = []
    other = []
    for wayEl in element.findall('way'):
        kArr = []
        vArr = []
        streetName = ''
        for tag in wayEl.findall('tag'):
            kArr.append(tag.get('k'))
            vArr.append(tag.get('v'))
            if tag.get('k') == 'name':
                streetName = tag.get('v')

        if 'highway' in kArr and ('residential' in vArr or 'secondary' in vArr or 'primary' in vArr or 'unclassified' in vArr):
            way = Way(streetName)

            for ref in wayEl.findall('nd'):
                refId = ref.get('ref')
                refNode = element.find(f'node[@id="{refId}"]')
                wayNode = Node(float(refNode.get('lat')), float(refNode.get('lon')), refId)
                nodesAll.append(wayNode)
                nodesWays.append(wayNode)
                way.addNode(wayNode)

            if 'residential' in vArr:
                residential.append(way)
            elif 'secondary' in vArr:
                secondary.append(way)
            elif 'primary' in vArr:
                primary.append(way)
            else:
                other.append(way)


    return residential, secondary, primary, other

def getMinMaxLonLat(nodesAll):
    lonArr = []
    latArr = []

    for node in nodesAll:
        lonArr.append(node.lon)
        latArr.append(node.lat)

    return min(lonArr), max(lonArr), min(latArr), max(latArr)

def drawWays(img, waysArr, color, minLon, deltaLon, minLat, deltaLat):
    for way in waysArr:
        if len(way.nodes) < 2:
            continue
        previous_node = way.nodes[0]
        for node in way.nodes[1:]:
            prev_x = int((previous_node.lon - minLon) / deltaLon * 1000)
            prev_y = int((previous_node.lat - minLat) / deltaLat * 1000)
            x = int((node.lon - minLon) * 1000 / deltaLon)
            y = int((node.lat - minLat) * 1000 / deltaLat)
            cv2.line(img, (prev_x, prev_y), (x, y), color, 1)
            previous_node = node

def drawStops(img, stopsArr, color, minLon, deltaLon, minLat, deltaLat):
    for stop in stopsArr:
        x = int((stop.node.lon - minLon) * 1000 / deltaLon)
        y = int((stop.node.lat - minLat) * 1000 / deltaLat)
        cv2.circle(img, (x, y), 5, color, -1)

def drawPoint(img, point, color, minLon, deltaLon, minLat, deltaLat):
    x = int((point.lon - minLon) * 1000 / deltaLon)
    y = int((point.lat - minLat) * 1000 / deltaLat)
    cv2.circle(img, (x, y), 5, color, -1)


root = ET.parse('/Users/matejnevlud/github/gis/poruba2.osm.xml').getroot()
nodesAll = []
nodesWays = []

stopsArr = getAllStops(root, nodesAll)
print(len(stopsArr))

residentialArr, secondaryArr, primaryArr, other = getWays(root, nodesAll, nodesWays)
print(len(residentialArr))
print(len(secondaryArr))

minLon, maxLon, minLat, maxLat = getMinMaxLonLat(nodesAll)
print(minLon, ' ', maxLon, ' ', minLat, ' ', maxLat)

deltaLon = maxLon - minLon
deltaLat = maxLat - minLat


image = np.zeros([1000,1000,3],dtype=np.uint8)
image.fill(255)

drawWays(image, residentialArr, (0, 255, 0), minLon, deltaLon, minLat, deltaLat)
drawWays(image, secondaryArr, (255, 0, 0), minLon, deltaLon, minLat, deltaLat)
drawWays(image, primaryArr, (255, 255, 0), minLon, deltaLon, minLat, deltaLat)
drawWays(image, other, (255, 0, 255), minLon, deltaLon, minLat, deltaLat)
drawStops(image, stopsArr, (0, 0, 255), minLon, deltaLon, minLat, deltaLat)

endLon = 0
endLat = 0

waysAllArr = residentialArr + secondaryArr + primaryArr
print(len(waysAllArr))



g = Graph()

for node in nodesWays:
    g.add_vertex(node.id)

for way in waysAllArr:
    prevNode = way.nodes[0]

    for node in way.nodes[1:]:
        width = math.sqrt( (node.lon - prevNode.lon)**2 + (node.lat - prevNode.lat)**2 )
        #print(width)
        g.add_edge(prevNode.id, node.id, width)
        prevNode = node


viet = []
pol = []
fei = []

for way in waysAllArr:
    if way.streetName == 'Vietnamská':
        for node in way.nodes:
            viet.append([node.lon, node.lat, node.id])
            p = Node(node.lat, node.lon)
           # drawPoint(image, p, (0, 120, 120), minLon, deltaLon, minLat, deltaLat)
    elif way.streetName == 'Polská':
        for node in way.nodes:
            pol.append([node.lon, node.lat, node.id])
    elif way.streetName == '17. listopadu':
        for node in way.nodes:
            fei.append([node.lon, node.lat, node.id])
            '''p = Node(node.lat, node.lon)
            drawPoint(image, p, (0, 120, 120), minLon, deltaLon, minLat, deltaLat)'''


feiSorted = sorted(fei , key=lambda k: [k[1], k[0]])
index = len(feiSorted)//2 - 1

pStart = Node(feiSorted[index][1], feiSorted[index][0], feiSorted[index][2])
drawPoint(image, pStart, (0, 120, 120), minLon, deltaLon, minLat, deltaLat)


endP = []

for i in viet:
    if i in pol:
        endP.append(i)

endLon = endP[0][0]
endLat = endP[0][1]

pEnd = Node(endLat, endLon, endP[0][2])

drawPoint(image, pEnd, (0, 0, 0), minLon, deltaLon, minLat, deltaLat)

dijkstra(g, g.get_vertex(pStart.id), g.get_vertex(pEnd.id))

target = g.get_vertex(pEnd.id)
path = [target.get_id()]
shortest(target, path)
print('The shortest : %s' %(path[::-1]))


pathArr = []

previous_node = pStart

for id in path[::-1]:
    if id == pStart.id:
        continue

    for node in nodesWays:
        if id == node.id:
            prev_x = int((previous_node.lon - minLon) / deltaLon * 1000)
            prev_y = int((previous_node.lat - minLat) / deltaLat * 1000)
            x = int((node.lon - minLon) * 1000 / deltaLon)
            y = int((node.lat - minLat) * 1000 / deltaLat)
            cv2.line(image, (prev_x, prev_y), (x, y), (0, 0, 0), 1)
            previous_node = node


image = cv2.flip(image, 0)
cv2.imshow('gis8.png', image)
cv2.waitKey(0)







print('img writed')




'''cv2.imshow('image',image)
cv2.waitKey(0)'''
