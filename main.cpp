#include <stdio.h>
#include <stdlib.h>

#include <cmath>

#include <opencv2/opencv.hpp>
#include <fstream>

//#include "<proj_api.h>"

#include "defines.h"
#include "utils.h"

#define STEP1_WIN_NAME "Heightmap"
#define STEP2_WIN_NAME "Edges"
#define ZOOM           1


struct MouseProbe {
    cv::Mat & heightmap_8uc1_img_;
    cv::Mat & heightmap_show_8uc3_img_;
    cv::Mat & edgemap_8uc1_img_;

    MouseProbe( cv::Mat & heightmap_8uc1_img, cv::Mat & heightmap_show_8uc3_img, cv::Mat & edgemap_8uc1_img )
     : heightmap_8uc1_img_( heightmap_8uc1_img ), heightmap_show_8uc3_img_( heightmap_show_8uc3_img ), edgemap_8uc1_img_( edgemap_8uc1_img )
    {
    }
};

// variables

// function declarations
void flood_fill( cv::Mat & src_img, cv::Mat & dst_img, const int x, const int y );


/**
 * Mouse clicking callback.
 */
void mouse_probe_handler( int event, int x, int y, int flags, void* param ) {
    MouseProbe *probe = (MouseProbe*)param;

    switch ( event ) {

        case cv::EVENT_LBUTTONDOWN:
            printf( "Clicked LEFT at: [ %d, %d ]\n", x, y );
            flood_fill( probe->edgemap_8uc1_img_, probe->heightmap_show_8uc3_img_, x, y );
            break;

        case cv::EVENT_RBUTTONDOWN:
            printf( "Clicked RIGHT at: [ %d, %d ]\n", x, y );
            break;
    }
}


void create_windows( const int width, const int height ) {
    cv::namedWindow( STEP1_WIN_NAME, 0 );
    cv::namedWindow( STEP2_WIN_NAME, 0 );

    cv::resizeWindow( STEP1_WIN_NAME, width*ZOOM, height*ZOOM );
    cv::resizeWindow( STEP2_WIN_NAME, width*ZOOM, height*ZOOM );

} // create_windows



/**
 * Transforms the image so it contains only two values.
 * Threshold may be set experimentally.
 */
void binarize_image( cv::Mat & src_8uc1_img ) {
    auto binarized = cv::Mat( cv::Size(src_8uc1_img.cols, src_8uc1_img.rows), CV_8UC1 );

    for (int y = 0; y < src_8uc1_img.rows; ++y) {
        for (int x = 0; x < src_8uc1_img.cols; ++x) {
            if (src_8uc1_img.at<uchar>(y, x) >= 128) {
                binarized.at<uchar>(y, x) = 255;
            } else {
                binarized.at<uchar>(y, x) = 0;
            }
        }
    }

    src_8uc1_img = binarized;
}


/**
 * Perform flood fill from the specified point (x, y) for the neighborhood points if they contain the same value,
 * as the one that came in argument 'value'. Function recursicely call itself for its 4-neighborhood.
 *
 * edgemap_8uc1_img - image, in which we perform flood filling
 * heightmap_show_8uc3_img - image, in which we display the filling
 * value - value, for which we'll perform flood filling
 */
void fill_step( cv::Mat & edgemap_8uc1_img, cv::Mat & heightmap_show_8uc3_img, const int x, const int y, const uchar value, const int stopCounter  = 10000) {
    int width, height;

    if (y < 0 || y > edgemap_8uc1_img.rows) return;
    if (x < 0 || x > edgemap_8uc1_img.cols) return;
    if(0 >= stopCounter) return;


    uchar ref_val = edgemap_8uc1_img.at<uchar>(y, x);

    if(ref_val == 128) return;

    if (ref_val == value){
        edgemap_8uc1_img.at<uchar>(y, x) = 128;
        heightmap_show_8uc3_img.at<cv::Vec3b>(y, x)[0] = 255;


        fill_step(edgemap_8uc1_img , heightmap_show_8uc3_img,   x,    y + 1,   value, stopCounter - 1);
        fill_step(edgemap_8uc1_img , heightmap_show_8uc3_img,   x,    y - 1,   value, stopCounter - 1);
        fill_step(edgemap_8uc1_img , heightmap_show_8uc3_img, x + 1,  y,   value, stopCounter - 1);
        fill_step(edgemap_8uc1_img , heightmap_show_8uc3_img, x - 1,  y,   value, stopCounter - 1);
    }

    return;

} //fill_step


/**
 * Perform flood fill from the specified point (x, y). The function remembers the value at the coordinate (x, y)
 * and fill the neighborhood using 'fill_step' function so long as the value in the neighborhood points are the same.
 * Execute the fill on a temporary image to prevent the original image from being repainted.

 * edgemap_8uc1_img - image, in which we perform flood filling
 * heightmap_show_8uc3_img - image, in which we display the filling
 */
void flood_fill( cv::Mat & edgemap_8uc1_img, cv::Mat & heightmap_show_8uc3_img, const int x, const int y ) {
    cv::Mat tmp_edgemap_8uc1_img;

    uchar ref_val = edgemap_8uc1_img.at<uchar>(y, x);
    fill_step(edgemap_8uc1_img , heightmap_show_8uc3_img, x, y, ref_val);

    binarize_image(edgemap_8uc1_img);


} //flood_fill



#pragma pack(1)
struct BinaryData {
    float x;
    float y;
    float z;
    int l_type;
};

/**
 * Find the minimum and maximum coordinates in the file.
 * Note that the file is the S-JTSK coordinate system.
 */
void get_min_max( const char *filename, float *a_min_x, float *a_max_x, float *a_min_y, float *a_max_y, float *a_min_z, float *a_max_z ) {
    FILE *f = NULL;
    float x, y, z;
    float min_x = 0.0f, min_y = 0.0f, min_z = 0.0f, max_x = -10.0f, max_y = 0.0f, max_z = 0.0f;
    int l_type;

    std::ifstream fileStream;
    fileStream.open(filename);

    printf("Opening filestream\n");

    bool isFirstRun = true;
    while (fileStream && !fileStream.eof()){
        BinaryData data;
        fileStream.read(reinterpret_cast<char *>(&data), sizeof(BinaryData));

        if (isFirstRun) {
            min_x = max_x = data.x;
            min_y = max_y = data.y;
            min_z = max_z = data.z;
            isFirstRun = false;
        }

        if(data.x < min_x) min_x = data.x;
        if(data.x > max_x) max_x = data.x;

        if(data.y < min_y) min_y = data.y;
        if(data.y > max_y) max_y = data.y;

        if(data.z < min_z) min_z = data.z;
        if(data.z > max_z) max_z = data.z;

    }

    printf("Closing filestream\n");
    fileStream.close();

    *a_min_x = min_x;
    *a_min_y = min_y;
    *a_min_z = min_z;

    *a_max_x = max_x;
    *a_max_y = max_y;
    *a_max_z = max_z;

}


/**
 * Fill the image by data from lidar.
 * All lidar points are stored in a an array that has the dimensions of the image. Then the pixel is assigned
 * a value as an average value range from at the corresponding array element. However, with this simple data access, you will lose data precission.
 * filename - file with binarny data
 * img - input image
 */
void fill_image( const char *filename, cv::Mat & heightmap_8uc1_img, float min_x, float max_x, float min_y, float max_y, float min_z, float max_z, int wanted_ltype = -1 ) {
    FILE *f = NULL;
    int delta_x, delta_y, delta_z;
    float fx, fy, fz;
    int x, y, l_type;
    int stride;
    int num_points = 1;
    float range = 0.0f;
    float *sum_height = NULL;
    int *sum_height_count = NULL;

    // zjistime sirku a vysku obrazu
    delta_x = round( max_x - min_x + 0.5f );
    delta_y = round( max_y - min_y + 0.5f );
    delta_z = round( max_z - min_z + 0.5f );

    stride = delta_x;

    //auto helper_map = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_32FC2 );

    sum_height = new float [heightmap_8uc1_img.rows * heightmap_8uc1_img.cols];
    sum_height_count = new int [heightmap_8uc1_img.rows * heightmap_8uc1_img.cols];

    std::ifstream fileStream;
    fileStream.open(filename);
    printf("Opening filestream\n");

    bool isFirstRun = true;
    while (fileStream && !fileStream.eof()){
        BinaryData data;
        fileStream.read(reinterpret_cast<char *>(&data), sizeof(BinaryData));

        if(data.l_type != wanted_ltype && wanted_ltype != -1) continue;

        x = cvRound(data.x - min_x);
        y = cvRound(data.y - min_y);
        int z = cvRound(data.z - min_z);

        sum_height[y * heightmap_8uc1_img.cols + x] += z;
        sum_height_count[y * heightmap_8uc1_img.cols + x] += 1;
    }

    printf("Closing filestream\n");
    fileStream.close();


    int maxSum = heightmap_8uc1_img.rows * heightmap_8uc1_img.cols;
    for (int i = 1; i < heightmap_8uc1_img.rows * heightmap_8uc1_img.cols; ++i) {
        int z = sum_height[i] / sum_height_count[i];
        int normalized_z = cvRound((float) z  * (255.0f / (float) delta_z));

        x = i % heightmap_8uc1_img.cols;
        y = floor(i / heightmap_8uc1_img.cols);
        heightmap_8uc1_img.at<uchar>( y , x ) = normalized_z;
    }

    cv::flip(heightmap_8uc1_img, heightmap_8uc1_img, 0);


    delete[] sum_height;
    delete[] sum_height_count;
}


void make_edges( const cv::Mat & src_8uc1_img, cv::Mat & edgemap_8uc1_img ) {
    cv::Canny( src_8uc1_img, edgemap_8uc1_img, 1, 80 );

    auto eroded = cv::Mat( cv::Size(src_8uc1_img.cols, src_8uc1_img.rows), CV_8UC1 );

    for (int y = 1; y < src_8uc1_img.rows - 1; ++y) {
        for (int x = 1; x < src_8uc1_img.cols - 1; ++x) {
            eroded.at<uchar>(y, x) = 0;
        }
    }

}



void dilate_and_erode_edgemap( cv::Mat & edgemap_8uc1_img ) {
    auto dilated = cv::Mat( cv::Size(edgemap_8uc1_img.cols, edgemap_8uc1_img.rows), CV_8UC1 );

    for (int y = 1; y < edgemap_8uc1_img.rows - 1; ++y) {
        for (int x = 1; x < edgemap_8uc1_img.cols - 1; ++x) {
            if (edgemap_8uc1_img.at<uchar>(y, x) == 255) {
                dilated.at<uchar>(y, x) = 255;
                dilated.at<uchar>(y - 1, x) = 255;
                dilated.at<uchar>(y + 1, x) = 255;
                dilated.at<uchar>(y, x + 1) = 255;
                dilated.at<uchar>(y - 1, x + 1) = 255;
                dilated.at<uchar>(y + 1, x + 1) = 255;
                dilated.at<uchar>(y, x - 1) = 255;
                dilated.at<uchar>(y - 1, x - 1) = 255;
                dilated.at<uchar>(y + 1, x - 1) = 255;
            }
        }
    }

    auto eroded = cv::Mat( cv::Size(edgemap_8uc1_img.cols, edgemap_8uc1_img.rows), CV_8UC1 );

    for (int y = 1; y < dilated.rows - 1; ++y) {
        for (int x = 1; x < dilated.cols - 1; ++x) {
            if (dilated.at<uchar>(y, x) == 255) {
                int sum = dilated.at<uchar>(y, x) +
                dilated.at<uchar>(y - 1, x) +
                dilated.at<uchar>(y + 1, x) +
                dilated.at<uchar>(y, x + 1) +
                dilated.at<uchar>(y - 1, x + 1) +
                dilated.at<uchar>(y + 1, x + 1) +
                dilated.at<uchar>(y, x - 1) +
                dilated.at<uchar>(y - 1, x - 1) +
                dilated.at<uchar>(y + 1, x - 1);

                if (sum >= 2295) eroded.at<uchar>(y, x) = 255;
                else eroded.at<uchar>(y, x) = 0;
            } else eroded.at<uchar>(y, x) = 0;
        }
    }

    edgemap_8uc1_img = eroded;

}


void process_lidar( const char *txt_filename, const char *bin_filename, const char *img_filename ) {
    float min_x, max_x, min_y, max_y, min_z, max_z;
    float delta_x, delta_y, delta_z;
    MouseProbe *mouse_probe;

    cv::Mat heightmap_8uc1_img;      // image of source of lidar data
    cv::Mat heightmap_show_8uc3_img; // image to detected areas
    cv::Mat edgemap_8uc1_img;        // image for edges

    get_min_max( bin_filename, &min_x, &max_x, &min_y, &max_y, &min_z, &max_z );

    printf( "min x: %f, max x: %f\n", min_x, max_x );
    printf( "min y: %f, max y: %f\n", min_y, max_y );
    printf( "min z: %f, max z: %f\n", min_z, max_z );

    delta_x = max_x - min_x;
    delta_y = max_y - min_y;
    delta_z = max_z - min_z;

    printf( "delta x: %f\n", delta_x );
    printf( "delta y: %f\n", delta_y );
    printf( "delta z: %f\n", delta_z );

    // create images according to data from the source file

    heightmap_8uc1_img = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_8UC1 );
    heightmap_show_8uc3_img = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_8UC3 );
    edgemap_8uc1_img = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_8UC3 );

    create_windows( heightmap_8uc1_img.cols, heightmap_8uc1_img.rows );
    mouse_probe = new MouseProbe( heightmap_8uc1_img, heightmap_show_8uc3_img, edgemap_8uc1_img );

    cv::setMouseCallback( STEP1_WIN_NAME, mouse_probe_handler, mouse_probe );
    cv::setMouseCallback( STEP2_WIN_NAME, mouse_probe_handler, mouse_probe );

    printf( "Image w=%d, h=%d\n", heightmap_8uc1_img.cols, heightmap_8uc1_img.rows );


    // fill the image with data from lidar scanning
    fill_image( bin_filename, heightmap_8uc1_img, min_x, max_x, min_y, max_y, min_z, max_z, 0);
    cv::cvtColor( heightmap_8uc1_img, heightmap_show_8uc3_img, cv::COLOR_GRAY2RGB );

    auto heightmap_ltype_0 = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_8UC1 );
    fill_image( bin_filename, heightmap_ltype_0, min_x, max_x, min_y, max_y, min_z, max_z, 0 );
    cv::imshow( "ltype_0", heightmap_ltype_0 );
    auto heightmap_ltype_1 = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_8UC1 );
    fill_image( bin_filename, heightmap_ltype_1, min_x, max_x, min_y, max_y, min_z, max_z, 1 );
    cv::imshow( "ltype_1", heightmap_ltype_1 );
    auto heightmap_ltype_2 = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_8UC1 );
    fill_image( bin_filename, heightmap_ltype_2, min_x, max_x, min_y, max_y, min_z, max_z, 2 );
    cv::imshow( "ltype_2", heightmap_ltype_2 );

    // create edge map from the height image
    make_edges( heightmap_8uc1_img, edgemap_8uc1_img );

    // binarize image, so we can easily process it in the next step
    binarize_image( edgemap_8uc1_img );

    // implement image dilatation and erosion
    dilate_and_erode_edgemap( edgemap_8uc1_img );


    auto heightmap_rgba_8uc4_img = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_8UC4 );
    //cv::cvtColor( heightmap_show_8uc3_img, heightmap_rgba_8uc4_img, cv::COLOR_RGB2RGBA );
    for (int y = 0; y < heightmap_rgba_8uc4_img.rows; ++y) {
        for (int x = 0; x < heightmap_rgba_8uc4_img.cols; ++x) {
            uchar val = heightmap_8uc1_img.at<uchar>(y,x);
            if (val > 0)
                heightmap_rgba_8uc4_img.at<cv::Vec4b>(y,x) = {val, val, val, 255};
        }
    }

    cv::imwrite( img_filename, heightmap_rgba_8uc4_img );

    // wait here for user input using (mouse clicking)
    int wanted_l_type = 0;

    while ( 1 ) {
        cv::imshow( STEP1_WIN_NAME, heightmap_show_8uc3_img );
        cv::imshow( STEP2_WIN_NAME, edgemap_8uc1_img );
        int key = cv::waitKey( 10 );
        if ( key == 'q' ) {
            break;
        }

        if ( key == 'h' ) {
            auto new_heightmap_8uc1_img = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_8UC1 );
            fill_image( bin_filename, new_heightmap_8uc1_img, min_x, max_x, min_y, max_y, min_z, max_z, 2 );
            cv::imshow( "h", new_heightmap_8uc1_img );
        }
        if ( key == 'j' ) {
            auto new_heightmap_8uc1_img = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_8UC1 );
            fill_image( bin_filename, new_heightmap_8uc1_img, min_x, max_x, min_y, max_y, min_z, max_z, 0 );
            cv::cvtColor( new_heightmap_8uc1_img, heightmap_show_8uc3_img, cv::COLOR_GRAY2RGB );
        }
        if ( key == 'k' ) {
            auto new_heightmap_8uc1_img = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_8UC1 );
            fill_image( bin_filename, new_heightmap_8uc1_img, min_x, max_x, min_y, max_y, min_z, max_z, 1 );
            cv::cvtColor( new_heightmap_8uc1_img, heightmap_show_8uc3_img, cv::COLOR_GRAY2RGB );
        }
        if ( key == 'l' ) {
            auto new_heightmap_8uc1_img = cv::Mat( cv::Size( cvRound( delta_x + 0.5f ), cvRound( delta_y + 0.5f ) ), CV_8UC1 );
            fill_image( bin_filename, new_heightmap_8uc1_img, min_x, max_x, min_y, max_y, min_z, max_z, 2 );
            cv::cvtColor( new_heightmap_8uc1_img, heightmap_show_8uc3_img, cv::COLOR_GRAY2RGB );
        }
    }

}


int main( int argc, char *argv[] ) {
    char *txt_file, *bin_file, *img_file;

    printf("argc: %d\n", argc );


    txt_file = "../pt000023.txt";
    bin_file = "../pt000023.bin";
    img_file = "../pt000023.png";

    process_lidar( txt_file, bin_file, img_file );


    return 0;
}
