import xml.etree.ElementTree as ET
from PIL import Image, ImageDraw


class Node:
    def __init__(self, lat, lon):
        self.lat = float(lat)
        self.lon = float(lon)

class Stop:
    def __init__(self, node, name):
        self.node = node
        self.name = name

class Way:
    def __init__(self, nodes = []):
        self.nodes = nodes

    def addNode(self, node):
        self.nodes.append(node)





root = ET.parse('/Users/matejnevlud/github/gis/poruba2.osm.xml').getroot()


stops = []
for node in root.findall('node'):
    lat = node.get('lat')
    lon = node.get('lon')

    #print(lat, lon)
    tags = node.findall('tag')

    v_data = []
    k_data = []
    for tag in tags:
        k_data.append(tag.get('k'))
        v_data.append(tag.get('v'))

    if 'stop_position' in v_data and 'name' in k_data:
        stops.append(Stop(Node(lat, lon), v_data[k_data.index('name')]))
print('Found', len(stops), 'stops')


lats = []
longs = []
residential = []
secondary = []
for way in root.findall('way'):
    lat = way.get('lat')
    lon = way.get('lon')


    v_data = []
    k_data = []
    for tag in way.findall('tag'):
        k_data.append(tag.get('k'))
        v_data.append(tag.get('v'))

    if 'highway' not in k_data:
        continue

    if 'residential' not in v_data and 'secondary' not in v_data:
        continue


    way_obj = Way([])

    for ref in way.findall('nd'):
        ref_id = ref.get('ref')
        ref_node = root.find(f'node[@id="{ref_id}"]')

        way_obj.addNode(Node(ref_node.get('lat'), ref_node.get('lon')))
        lats.append(float(ref_node.get('lat')))
        longs.append(float(ref_node.get('lon')))


    if 'residential' in v_data:
        residential.append(way_obj)

    if 'secondary' in v_data:
        secondary.append(way_obj)


print('Found', len(residential), 'residential and', len(secondary), 'secondary ways')


min_y = min(lats)
max_y = max(lats)
min_x = min(longs)
max_x = max(longs)
delta_y = max_y - min_y
delta_x = max_x - min_x

print('Min lat:', min_y, 'Max lat:', max_y, 'Min lon:', min_x, 'Max lon:', max_x)


map = Image.new('RGB', (1001, 1001), (255, 255, 255))
map_draw = ImageDraw.Draw(map)

for way in residential:
    if len(way.nodes) < 2:
        continue
    previous_node = way.nodes[0]
    for node in way.nodes[1:]:
        prev_x = (previous_node.lon - min_x) / delta_x * 1000
        prev_y = (previous_node.lat - min_y) / delta_y * 1000
        x = int((node.lon - min_x) * 1000 / delta_x)
        y = int((node.lat - min_y) * 1000 / delta_y)
        map_draw.line([prev_x, prev_y, x, y], fill='green')
        previous_node = node

for way in secondary:
    if len(way.nodes) < 2:
        continue
    previous_node = way.nodes[0]
    for node in way.nodes[1:]:
        prev_x = (previous_node.lon - min_x) / delta_x * 1000
        prev_y = (previous_node.lat - min_y) / delta_y * 1000
        x = int((node.lon - min_x) * 1000 / delta_x)
        y = int((node.lat - min_y) * 1000 / delta_y)
        map_draw.line([prev_x, prev_y, x, y], fill='red')
        previous_node = node


for stop in stops:
    x = (stop.node.lon - min_x) * 1000 / delta_x
    y = (stop.node.lat - min_y) * 1000 / delta_y
    map_draw.ellipse([x - 5, y - 5, x + 5, y + 5], fill=(0, 0, 255))
    #map_draw.text([x - 10, y - 10], stop.name, fill=(0, 0, 0))

map.show()

